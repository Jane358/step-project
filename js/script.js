$(document).ready(function () {

  $(function () {
    $('ul.tabs__caption').on('click', 'li:not(.active-tab)', function () {
      $(this)
        .addClass('active-tab').siblings().removeClass('active-tab')
        .closest('div.tabs').find('div.tabs__content').removeClass('active-tab').eq($(this).index()).addClass('active-tab');
    });
  });


  $('li.work-gallery-item').on('click', function () {
    $(this)
      .addClass('active-item').siblings().removeClass('active-item');
  });


  $('.work-gallery-link').on('click', function (event) {
    event.preventDefault();
    let projects = $(this).closest('.work-gallery-list').next();
    let attr = $(this).data('attr');
    let visible_element = '.' + attr;

    if (attr == 'all-projects') {
      $('.tiles').children('.tile').hide();
      $('.tiles').children('.tile').removeClass('active');
      $('.tile').slice(0, 12).addClass('active').show();
      $('.work-gallery-button').fadeIn('slow');
    }
    else {
      projects.children('div').show();
      projects.children('div:not(' + visible_element + ')').hide();
      projects.children('div:not(' + visible_element + ')').removeClass('active');
      projects.children('div ' + visible_element + '').addClass('active');
      if ($('.tile.active').length < 13) {
        $('.work-gallery-button').fadeOut('slow');
      } else {
        projects.children(visible_element).slice(12).hide();
        $('.work-gallery-button').fadeIn('slow');
      }
    };
  })


  $('.work-gallery-button').on('click', function (event) {
    event.preventDefault();
    $(this).append('<span class="preloader"></span>');
    setTimeout(function () {
      $('.preloader').remove();
      if ($('.active-item a').data('attr') ==  'all-projects') {
        loadMoreAll(12);
      } else {
        loadMoreOther(12);
      }
    }, 2000);
  })


  function loadMoreAll(number) {
    let target = $('.tile');
    let targetNotActive = $(target).not('.active');
    targetNotActive.slice(0, number).addClass('active').show();
    targetNotActive = $(target).not('.active');
    if (targetNotActive.length == 0) {
      $('.work-gallery-button').fadeOut('slow');
    }
  }

  function loadMoreOther(number) {
    let targetNotVisible = $('.tile.active[style="display: none;"]');
    targetNotVisible.slice(0, number).show();
    if (targetNotVisible.length < 13) {
      $('.work-gallery-button').fadeOut('slow');
    }
  }


  $('.slider-for, .slider-nav').on('init', function (slick, event) {
    $(this).css('visibility', 'visible');
  });


  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });


  let grid = $('.grid');
  let masonryOptions = {
    itemSelector: '.grid-item',
    columnWidth: 370,
    gutter: 15
  };

  grid.masonry(masonryOptions);


  $('.images-gallery-btn').on('click', function (event) {
    event.preventDefault();
    $(this).append('<span class="preloader"></span>');
    setTimeout(function () {
      $('.preloader').remove();
      loadMoreImages(8);
      grid.masonry(masonryOptions);
    }, 2000);
  })

  function loadMoreImages(number) {
    let target = $('.grid-item');
    let targetNotActive = $(target).not('.active-grid');
    targetNotActive.slice(0, number).addClass('active-grid');
    targetNotActive = $(target).not('.active-grid');
    if (targetNotActive.length == 0) {
      $('.images-gallery-btn').fadeOut('slow');
    }
  };
});